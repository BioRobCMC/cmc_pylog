import setuptools

setuptools.setup(
    name='cmc_pylog',
    version='2019a',
    description='Python logger for cmc framework',
    url='https://gitlab.epfl.ch/BioRobCMC/cmc_pylog.git',
    author='biorob-cmc',
    author_email='biorob-cmc@groupes.epfl.ch',
    license='MIT',
    packages=setuptools.find_packages(),
    install_requires=['colorama'],
    zip_safe=False
)
